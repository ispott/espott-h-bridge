<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="1" unitdist="mm" unit="mm" style="lines" multiple="1" display="yes" altdistance="0.1" altunitdist="mm" altunit="mm"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="yes" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="yes" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="yes" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="yes" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="yes" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="yes" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="yes" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="yes" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="yes" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="yes" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="2021-11-25_22-34-28_Library">
<packages>
<package name="DIODE_1N5408G-T_DIO" urn="urn:adsk.eagle:footprint:1/1">
<pad name="1" x="0" y="0" drill="1.5494" diameter="2.0574" shape="square"/>
<pad name="2" x="19.9" y="0" drill="1.5494" diameter="2.0574" rot="R180"/>
<wire x1="-2.5527" y1="0" x2="-1.3614" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.9177" y1="0.635" x2="-1.9177" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.0732" y1="-2.7813" x2="14.8268" y2="-2.7813" width="0.1524" layer="21"/>
<wire x1="14.8268" y1="-2.7813" x2="14.8268" y2="2.7813" width="0.1524" layer="21"/>
<wire x1="14.8268" y1="2.7813" x2="5.0732" y2="2.7813" width="0.1524" layer="21"/>
<wire x1="5.0732" y1="2.7813" x2="5.0732" y2="-2.7813" width="0.1524" layer="21"/>
<wire x1="-2.5527" y1="0" x2="-1.2827" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.9177" y1="0.635" x2="-1.9177" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="0" y1="0" x2="5.2002" y2="0" width="0.1524" layer="51"/>
<wire x1="19.9" y1="0" x2="14.6998" y2="0" width="0.1524" layer="51"/>
<wire x1="5.2002" y1="-2.6543" x2="14.6998" y2="-2.6543" width="0.1524" layer="51"/>
<wire x1="14.6998" y1="-2.6543" x2="14.6998" y2="2.6543" width="0.1524" layer="51"/>
<wire x1="14.6998" y1="2.6543" x2="5.2002" y2="2.6543" width="0.1524" layer="51"/>
<wire x1="5.2002" y1="2.6543" x2="5.2002" y2="-2.6543" width="0.1524" layer="51"/>
<text x="6.6788" y="-0.635" size="1.27" layer="25" ratio="6">&gt;Name</text>
<text x="8.2212" y="-0.635" size="1.27" layer="27" ratio="6">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="DIODE" urn="urn:adsk.eagle:symbol:2/1">
<pin name="2" x="0" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="1" x="10.16" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="2.54" y1="0" x2="3.4798" y2="0" width="0.2032" layer="94"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.2032" layer="94"/>
<wire x1="3.175" y1="0" x2="3.81" y2="0" width="0.2032" layer="94"/>
<wire x1="6.35" y1="-1.905" x2="6.35" y2="1.905" width="0.2032" layer="94"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.2032" layer="94"/>
<wire x1="6.35" y1="0" x2="3.81" y2="1.905" width="0.2032" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="6.35" y2="0" width="0.2032" layer="94"/>
<text x="-3.8831" y="-5.5499" size="3.48" layer="96" ratio="10">&gt;Value</text>
<text x="-2.8148" y="2.7051" size="3.48" layer="95" ratio="10">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="1N5408G-T" urn="urn:adsk.eagle:component:4/1" prefix="CR">
<gates>
<gate name="A" symbol="DIODE" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="DIODE_1N5408G-T_DIO" package="DIODE_1N5408G-T_DIO">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="1N5408GDICT-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="1N5408GDITR-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="1N5408G-T" constant="no"/>
<attribute name="MFR_NAME" value="Diodes Inc" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="2021-11-25_23-05-00_Library">
<packages>
<package name="TO_80AE-GE3_VIS" urn="urn:adsk.eagle:footprint:1/1">
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.3208"/>
<pad name="2" x="2.6" y="0" drill="0.8128" diameter="1.3208"/>
<pad name="3" x="5.2" y="0" drill="0.8128" diameter="1.3208"/>
<wire x1="-2.5562" y1="-0.0691" x2="-0.9907" y2="-0.0691" width="0.1524" layer="21"/>
<wire x1="-2.6832" y1="-2.1265" x2="7.8832" y2="-2.1265" width="0.1524" layer="21"/>
<wire x1="7.8832" y1="-2.1265" x2="7.8832" y2="2.8265" width="0.1524" layer="21"/>
<wire x1="7.8832" y1="2.8265" x2="-2.6832" y2="2.8265" width="0.1524" layer="21"/>
<wire x1="-2.6832" y1="2.8265" x2="-2.6832" y2="-2.1265" width="0.1524" layer="21"/>
<wire x1="0.9907" y1="-0.0691" x2="1.6093" y2="-0.0691" width="0.1524" layer="21"/>
<wire x1="3.5907" y1="-0.0691" x2="4.2093" y2="-0.0691" width="0.1524" layer="21"/>
<wire x1="6.1907" y1="-0.0691" x2="7.7562" y2="-0.0691" width="0.1524" layer="21"/>
<wire x1="-4.3342" y1="0" x2="-4.5882" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-4.5882" y1="0" x2="-4.3342" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.5562" y1="-0.1961" x2="7.7562" y2="-0.1961" width="0.1524" layer="51"/>
<wire x1="-2.5562" y1="-1.9995" x2="7.7562" y2="-1.9995" width="0.1524" layer="51"/>
<wire x1="7.7562" y1="-1.9995" x2="7.7562" y2="2.6995" width="0.1524" layer="51"/>
<wire x1="7.7562" y1="2.6995" x2="-2.5562" y2="2.6995" width="0.1524" layer="51"/>
<wire x1="-2.5562" y1="2.6995" x2="-2.5562" y2="-1.9995" width="0.1524" layer="51"/>
<wire x1="-2.1752" y1="0" x2="-2.4292" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-2.4292" y1="0" x2="-2.1752" y2="0" width="0" layer="51" curve="-180"/>
<text x="1.7356" y="0.9342" size="0.635" layer="51" ratio="4">TAB</text>
<text x="-0.6712" y="-0.285" size="1.27" layer="25" ratio="6">&gt;Name</text>
<text x="0.8712" y="-0.285" size="1.27" layer="27" ratio="6">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="TRANS_N-MOSFETN" urn="urn:adsk.eagle:symbol:2/1">
<pin name="D" x="15.24" y="17.78" visible="pin" length="short" direction="pas" rot="R270"/>
<pin name="S" x="15.24" y="-12.7" visible="pin" length="short" direction="pas" rot="R90"/>
<pin name="G" x="0" y="0" visible="pin" length="short" direction="pas"/>
<wire x1="2.54" y1="0" x2="4.445" y2="0" width="0.2032" layer="94"/>
<wire x1="12.065" y1="0" x2="12.065" y2="5.08" width="0.2032" layer="94"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="3.175" width="0.2032" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="1.27" width="0.2032" layer="94"/>
<wire x1="12.7" y1="3.81" x2="12.7" y2="5.08" width="0.2032" layer="94"/>
<wire x1="13.97" y1="2.54" x2="14.605" y2="2.54" width="0.2032" layer="94"/>
<wire x1="12.7" y1="0.635" x2="14.605" y2="0.635" width="0.2032" layer="94"/>
<wire x1="14.605" y1="0" x2="14.605" y2="2.54" width="0.2032" layer="94"/>
<wire x1="14.605" y1="0" x2="15.875" y2="0" width="0.2032" layer="94"/>
<wire x1="12.7" y1="4.445" x2="14.605" y2="4.445" width="0.2032" layer="94"/>
<wire x1="15.875" y1="3.175" x2="15.875" y2="5.08" width="0.2032" layer="94"/>
<wire x1="15.875" y1="0" x2="15.875" y2="1.905" width="0.2032" layer="94"/>
<wire x1="16.51" y1="3.175" x2="15.24" y2="3.175" width="0.2032" layer="94"/>
<wire x1="14.605" y1="5.08" x2="15.875" y2="5.08" width="0.2032" layer="94"/>
<wire x1="14.605" y1="4.445" x2="14.605" y2="5.08" width="0.2032" layer="94"/>
<wire x1="4.445" y1="0" x2="12.065" y2="0" width="0.1524" layer="94"/>
<wire x1="15.2654" y1="0" x2="15.24" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="10.16" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="7.62" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="6.35" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="8.89" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="6.35" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-2.54" x2="8.89" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-1.905" x2="8.89" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-1.905" x2="7.62" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-0.635" x2="6.35" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-0.635" x2="8.89" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-0.635" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="15.24" y1="15.24" x2="15.2654" y2="5.08" width="0.1524" layer="94"/>
<wire x1="15.2654" y1="5.08" x2="15.2146" y2="5.08" width="0.508" layer="94" curve="-180"/>
<wire x1="15.2146" y1="5.08" x2="15.2654" y2="5.08" width="0.508" layer="94" curve="-180"/>
<wire x1="15.2654" y1="0" x2="15.2146" y2="0" width="0.508" layer="94" curve="-180"/>
<wire x1="15.2146" y1="0" x2="15.2654" y2="0" width="0.508" layer="94" curve="-180"/>
<wire x1="14.6304" y1="0.635" x2="14.5796" y2="0.635" width="0.508" layer="94" curve="-180"/>
<wire x1="14.5796" y1="0.635" x2="14.6304" y2="0.635" width="0.508" layer="94" curve="-180"/>
<wire x1="25.4" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="94" curve="-180"/>
<polygon width="0.0254" layer="94">
<vertex x="13.97" y="3.175"/>
<vertex x="12.7" y="2.54"/>
<vertex x="13.97" y="1.905"/>
</polygon>
<polygon width="0.0254" layer="94">
<vertex x="16.51" y="1.905"/>
<vertex x="15.24" y="1.905"/>
<vertex x="15.875" y="3.175"/>
</polygon>
<text x="29.21" y="8.255" size="2.54" layer="95" ratio="10">&gt;Name</text>
<text x="29.21" y="3.81" size="2.54" layer="96" ratio="10">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SIHA11N80AE-GE3" urn="urn:adsk.eagle:component:3/1" prefix="U">
<gates>
<gate name="A" symbol="TRANS_N-MOSFETN" x="0" y="0"/>
</gates>
<devices>
<device name="TO_80AE-GE3_VIS" package="TO_80AE-GE3_VIS">
<connects>
<connect gate="A" pin="D" pad="2"/>
<connect gate="A" pin="G" pad="1"/>
<connect gate="A" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="742-SIHA11N80AE-GE3TR-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="742-SIHA11N80AE-GE3CT-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_3" value="742-SIHA11N80AE-GE3DKR-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="SIHA11N80AE-GE3" constant="no"/>
<attribute name="MFR_NAME" value="Vishay" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="espott-VoltageDividerLib">
<packages>
<package name="DC-MOTOR">
<wire x1="-3.81" y1="0" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="3.81" y2="0" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.79605" width="0.127" layer="21"/>
<text x="-1.524" y="-3.429" size="1.27" layer="27">12V</text>
<text x="-0.762" y="-0.635" size="1.27" layer="21">M</text>
<pad name="P$1" x="-3.81" y="0" drill="0.6"/>
<pad name="P$2" x="3.81" y="0" drill="0.6"/>
<text x="-2.413" y="2.286" size="0.6096" layer="25">DC-MOTOR</text>
</package>
</packages>
<symbols>
<symbol name="DC-MOTOR">
<wire x1="-7.62" y1="0" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="3.636725" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<text x="-3.556" y="-0.762" size="1.778" layer="94">M</text>
<wire x1="-7.62" y1="0" x2="-10.16" y2="0" width="0.254" layer="94"/>
<text x="-4.826" y="-6.096" size="1.778" layer="96">12V</text>
<text x="-7.366" y="4.572" size="1.778" layer="95">DC Motor</text>
<pin name="P$1" x="7.62" y="0" length="middle" rot="R180"/>
<pin name="P$2" x="-12.7" y="0" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DC-MOTOR">
<gates>
<gate name="G$1" symbol="DC-MOTOR" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="DC-MOTOR">
<connects>
<connect gate="G$1" pin="P$1" pad="P$2"/>
<connect gate="G$1" pin="P$2" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols" urn="urn:adsk.eagle:library:530">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:39417/1" library_version="1">
<description>&lt;h3&gt;Ground Supply (Earth Ground Symbol)&lt;/h3&gt;</description>
<pin name="3.3V" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-2.032" y1="0" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.524" x2="0.508" y2="-1.524" width="0.254" layer="94"/>
<text x="0" y="-1.778" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="VIN" urn="urn:adsk.eagle:symbol:39426/1" library_version="1">
<description>&lt;h3&gt;Input Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="VIN" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND2" urn="urn:adsk.eagle:component:39442/1" prefix="GND" library_version="1">
<description>&lt;h3&gt;Ground Supply (Earth Ground style)&lt;/h3&gt;
&lt;p&gt;Ground supply with a traditional "earth ground" symbol.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="GND" x="2.54" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VIN" urn="urn:adsk.eagle:component:39447/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;Input Voltage Supply&lt;/h3&gt;
&lt;p&gt;Generic voltage input supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="VIN" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="H-Bridge Library">
<packages>
<package name="SPDT-13">
<wire x1="0" y1="0" x2="8" y2="0" width="0.1524" layer="25"/>
<wire x1="8" y1="0" x2="8" y2="16" width="0.1524" layer="25"/>
<wire x1="8" y1="16" x2="0" y2="16" width="0.1524" layer="25"/>
<wire x1="0" y1="16" x2="0" y2="0" width="0.1524" layer="25"/>
<hole x="2" y="0" drill="1.2"/>
<hole x="5.3" y="0" drill="1.2"/>
<hole x="5.3" y="16" drill="1.2"/>
<hole x="2" y="16" drill="1.2"/>
<pad name="P$1" x="3.6" y="8" drill="1.2"/>
<pad name="P$3" x="3.6" y="12.7" drill="1.2"/>
<pad name="P$2" x="3.6" y="3.3" drill="1.2"/>
<text x="4.8" y="4" size="0.8128" layer="25">1</text>
<text x="4.8" y="8.6" size="0.8128" layer="25">2</text>
<text x="4.8" y="13.3" size="0.8128" layer="25">3</text>
<text x="1.5" y="17.5" size="0.8128" layer="25">SPDT-13</text>
</package>
</packages>
<symbols>
<symbol name="SPDT-13">
<circle x="0" y="0" radius="0.5" width="0.254" layer="94"/>
<circle x="5" y="2" radius="0.5" width="0.254" layer="94"/>
<circle x="5" y="-2" radius="0.5" width="0.254" layer="94"/>
<pin name="P$1" x="-5" y="0" length="middle"/>
<pin name="P$2" x="10" y="-2" length="middle" rot="R180"/>
<pin name="P$3" x="10" y="2" length="middle" rot="R180"/>
<text x="5.7" y="2.4" size="1.27" layer="95">1</text>
<text x="-1.7" y="0.4" size="1.27" layer="95">2</text>
<text x="5.8" y="-1.7" size="1.27" layer="95">3</text>
<wire x1="0" y1="0" x2="5.08" y2="2.032" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SPDT-13">
<gates>
<gate name="G$1" symbol="SPDT-13" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SPDT-13">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$3"/>
<connect gate="G$1" pin="P$3" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="CR1" library="2021-11-25_22-34-28_Library" deviceset="1N5408G-T" device="DIODE_1N5408G-T_DIO"/>
<part name="CR2" library="2021-11-25_22-34-28_Library" deviceset="1N5408G-T" device="DIODE_1N5408G-T_DIO"/>
<part name="U1" library="2021-11-25_23-05-00_Library" deviceset="SIHA11N80AE-GE3" device="TO_80AE-GE3_VIS"/>
<part name="U2" library="2021-11-25_23-05-00_Library" deviceset="SIHA11N80AE-GE3" device="TO_80AE-GE3_VIS"/>
<part name="U3" library="2021-11-25_23-05-00_Library" deviceset="SIHA11N80AE-GE3" device="TO_80AE-GE3_VIS"/>
<part name="U4" library="2021-11-25_23-05-00_Library" deviceset="SIHA11N80AE-GE3" device="TO_80AE-GE3_VIS"/>
<part name="U$1" library="espott-VoltageDividerLib" deviceset="DC-MOTOR" device=""/>
<part name="GND1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="U$3" library="H-Bridge Library" deviceset="SPDT-13" device=""/>
<part name="SUPPLY1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VIN" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="CR1" gate="A" x="-7.7" y="-31.7" smashed="yes">
<attribute name="VALUE" x="-22.5831" y="-35.2499" size="1.27" layer="96" ratio="10"/>
<attribute name="NAME" x="-12.5148" y="-31.9949" size="1.27" layer="95" ratio="10"/>
</instance>
<instance part="CR2" gate="A" x="17.7" y="-31.7" smashed="yes" rot="R180">
<attribute name="VALUE" x="33.5831" y="-34.1501" size="1.27" layer="96" ratio="10" rot="R180"/>
<attribute name="NAME" x="22.5148" y="-31.4051" size="1.27" layer="95" ratio="10" rot="R180"/>
</instance>
<instance part="U1" gate="A" x="-23" y="16" smashed="yes">
<attribute name="NAME" x="-18.79" y="28.255" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="-37.79" y="30.81" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="U2" gate="A" x="-23" y="-17" smashed="yes">
<attribute name="NAME" x="-19.79" y="-4.745" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="-37.79" y="-2.19" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="U3" gate="A" x="33" y="21" smashed="yes" rot="R180">
<attribute name="NAME" x="28.79" y="29.745" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="49.79" y="32.19" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="U4" gate="A" x="33" y="-12" smashed="yes" rot="R180">
<attribute name="NAME" x="27.79" y="-3.255" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="48.79" y="-0.81" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="U$1" gate="G$1" x="7" y="2" smashed="yes"/>
<instance part="GND1" gate="G$1" x="4.9" y="-36.3" smashed="yes">
<attribute name="VALUE" x="4.9" y="-38.078" size="1.27" layer="96" align="top-center"/>
</instance>
<instance part="U$3" gate="G$1" x="4.9" y="51" smashed="yes" rot="R270"/>
<instance part="SUPPLY1" gate="G$1" x="4.9" y="57.5" smashed="yes">
<attribute name="VALUE" x="4.9" y="60.294" size="1.27" layer="96" align="bottom-center"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="3.3V" class="0">
<segment>
<pinref part="CR1" gate="A" pin="1"/>
<pinref part="CR2" gate="A" pin="1"/>
<wire x1="2.46" y1="-31.7" x2="4.9" y2="-31.7" width="0.1524" layer="91"/>
<pinref part="GND1" gate="G$1" pin="3.3V"/>
<wire x1="4.9" y1="-31.7" x2="7.54" y2="-31.7" width="0.1524" layer="91"/>
<wire x1="4.9" y1="-33.76" x2="4.9" y2="-31.7" width="0.1524" layer="91"/>
<junction x="4.9" y="-31.7"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U4" gate="A" pin="S"/>
<pinref part="U3" gate="A" pin="D"/>
<wire x1="17.76" y1="0.7" x2="17.76" y2="3.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U2" gate="A" pin="D"/>
<pinref part="U1" gate="A" pin="S"/>
<wire x1="-7.76" y1="0.78" x2="-7.76" y2="3.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$2"/>
<wire x1="-5.7" y1="2" x2="-7.7" y2="2" width="0.1524" layer="91"/>
<junction x="-7.7" y="2"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$1"/>
<wire x1="14.62" y1="2" x2="17.7" y2="2" width="0.1524" layer="91"/>
<junction x="17.7" y="2"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="CR1" gate="A" pin="2"/>
<pinref part="U2" gate="A" pin="S"/>
<wire x1="-7.7" y1="-31.7" x2="-7.76" y2="-31.7" width="0.1524" layer="91"/>
<wire x1="-7.76" y1="-31.7" x2="-7.76" y2="-29.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="CR2" gate="A" pin="2"/>
<pinref part="U4" gate="A" pin="D"/>
<wire x1="17.7" y1="-31.7" x2="17.7" y2="-29.78" width="0.1524" layer="91"/>
<wire x1="17.7" y1="-29.78" x2="17.76" y2="-29.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U1" gate="A" pin="G"/>
<wire x1="-23" y1="16" x2="-28" y2="16" width="0.1524" layer="91"/>
<wire x1="-28" y1="16" x2="-28" y2="-41" width="0.1524" layer="91"/>
<pinref part="U4" gate="A" pin="G"/>
<wire x1="-28" y1="-41" x2="33" y2="-41" width="0.1524" layer="91"/>
<wire x1="33" y1="-41" x2="33" y2="-12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U2" gate="A" pin="G"/>
<wire x1="-23" y1="-17" x2="-23" y2="-45" width="0.1524" layer="91"/>
<wire x1="-23" y1="-45" x2="37" y2="-45" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="G"/>
<wire x1="37" y1="-45" x2="37" y2="21" width="0.1524" layer="91"/>
<wire x1="37" y1="21" x2="33" y2="21" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U1" gate="A" pin="D"/>
<pinref part="U$3" gate="G$1" pin="P$2"/>
<wire x1="-7.76" y1="33.78" x2="-7.76" y2="41" width="0.1524" layer="91"/>
<wire x1="-7.76" y1="41" x2="2.9" y2="41" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U3" gate="A" pin="S"/>
<pinref part="U$3" gate="G$1" pin="P$3"/>
<wire x1="17.76" y1="33.7" x2="17.76" y2="41" width="0.1524" layer="91"/>
<wire x1="17.76" y1="41" x2="6.9" y2="41" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="P$1"/>
<pinref part="SUPPLY1" gate="G$1" pin="VIN"/>
<wire x1="4.9" y1="57.5" x2="4.9" y2="56" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="106,1,-5.7,2,N$3,,,,,"/>
<approved hash="106,1,14.62,2,N$4,,,,,"/>
<approved hash="117,1,4.9,57.5,VIN,,,,,"/>
<approved hash="107,1,17.7,2,N$1,N$4,,,,"/>
<approved hash="107,1,-7.7,2,N$2,N$3,,,,"/>
<approved hash="110,1,17.7,2,N$1,N$4,,,,"/>
<approved hash="110,1,-7.7,2,N$2,N$3,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
